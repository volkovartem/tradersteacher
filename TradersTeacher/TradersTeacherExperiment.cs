﻿using SharpNeat.Core;
using SharpNeat.Phenomes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradersTeacher
{
    /// <summary>
    /// Defines the setup for the HandWritten evolution experiment.
    /// </summary>
    public class TradersTeacherExperiment : SimpleNeatExperiment
    {
        /// <summary>
        /// Gets the HandWritten evaluator that scores individuals.
        /// </summary>
        public override IPhenomeEvaluator<IBlackBox> PhenomeEvaluator
        {
            get { return new TradersTeacherEvaluator(); }
        }

        /// <summary>
        /// Defines the number of input nodes in the neural network.
        /// The network has one input for each pixel,
        /// so it has 98*98 inputs total.
        /// </summary>
        public override int InputCount
        {
            get { return Data.SizeOfCard * 6; }
        }

        /// <summary>
        /// Defines the number of output nodes in the neural network.
        /// The network has one output for each number,
        /// so it has 10 outputs total.
        /// </summary>
        public override int OutputCount
        {
            get { return Data.outputCount; }
        }

        /// <summary>
        /// Defines whether all networks should be evaluated every
        /// generation, or only new (child) networks. 
        /// We try to evaluate agents again every generation,
        /// to make sure it wasn't just luck.
        /// </summary>
        public override bool EvaluateParents
        {
            get { return true; }
        }
    }
}
