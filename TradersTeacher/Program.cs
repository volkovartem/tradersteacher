﻿using log4net.Config;
using Newtonsoft.Json;
using SharpNeat.EvolutionAlgorithms;
using SharpNeat.Genomes.Neat;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Xml;
using static TradersTeacher.Data;

namespace TradersTeacher
{
    class Program
    {
        static NeatEvolutionAlgorithm<NeatGenome> _ea;
        const string CHAMPION_FILE = "the_best_agent.xml";

        static void Main(string[] args)
        {
            //MyExperiment2();
            StartLearning();

            
        }

        static void ea_UpdateEvent(object sender, EventArgs e)
        {
            //textExperiment = Convert.ToInt32((DateTime.Now - startExperiment).TotalSeconds).ToString() + "," + _ea.CurrentGeneration.ToString();
            Console.WriteLine(string.Format("{2}  ::{4}::{5}::   gen={0:N0} bestFitness={1:N0}    goal {3}", _ea.CurrentGeneration, _ea.Statistics._maxFitness, DateTime.Now.ToString(), Distance * 3, Convert.ToInt32((DateTime.Now - startExperiment).TotalSeconds).ToString(), Coeff));
            
            // Save the best genome to file
            var doc = NeatGenomeXmlIO.SaveComplete(new List<NeatGenome>() { _ea.CurrentChampGenome }, false);
            doc.Save(CHAMPION_FILE);


        }

        private static List<Candle> ParsingCSV(string path, int format)
        {
            /// format 1 : ETHUSDT,20/08/2017 20:56,20/08/2017 20:57,O,H,L,C,V
            /// format 2 : 2016.01.04,00:00,O,H,L,C,V
            List<Candle> InputCSV = new List<Candle>();
            try
            {
                List<string> input_csv = new List<string>(File.ReadAllLines(path));

                foreach (string str in input_csv)
                {
                    string[] cndls = str.Split(',');
                    if (format == 1)
                    {
                        Candle cndl = new Candle
                        {
                            T = cndls[1].Split(' ')[1],
                            O = cndls[3],
                            H = cndls[4],
                            L = cndls[5],
                            C = cndls[6],
                            V = cndls[7]
                        };
                        InputCSV.Add(cndl);
                    }
                    if (format == 2)
                    {
                        Candle cndl = new Candle
                        {
                            T = cndls[1],
                            O = cndls[2],
                            H = cndls[3],
                            L = cndls[4],
                            C = cndls[5],
                            V = cndls[6]
                        };
                        InputCSV.Add(cndl);
                    }
                }
            }
            catch
            {
                Console.WriteLine("Something wrong with a parsing");
                Console.ReadKey(true);
            }
            return InputCSV;
        }

        private static void CreateDataBase(bool normalize = true)
        {
            DataBase = new List<Card>();
            DataBaseSell = new List<Card>();
            DataBaseHold = new List<Card>();
            DataBaseBuy = new List<Card>();

            if (InputCSV != null && InputCSV.Count > 0)
            {
                for (int j = 0; j <= (InputCSV.Count - SizeOfCard - ExpirationTime); j++)
                {
                    Card card = new Card();
                    card.Candles = new List<Candle>();
                    for (int i = 0; i < SizeOfCard; i++)
                    {
                        if (normalize)
                        {
                            card.Candles.Add(new Candle
                            {
                                T = NormalizeTime(InputCSV[j + i].T),
                                O = Normalize(InputCSV[j + i].O),
                                H = Normalize(InputCSV[j + i].H),
                                L = Normalize(InputCSV[j + i].L),
                                C = Normalize(InputCSV[j + i].C),
                                V = Normalize(InputCSV[j + i].V)
                            });
                        }
                        else
                        {
                            card.Candles.Add(new Candle
                            {
                                T = NormalizeTime(InputCSV[j + i].T),
                                O = InputCSV[j + i].O,
                                H = InputCSV[j + i].H,
                                L = InputCSV[j + i].L,
                                C = InputCSV[j + i].C,
                                V = Normalize(InputCSV[j + i].V)
                            });
                        }
                    }

                    card.Answer = GetAnswer(j + SizeOfCard - 1);
                    card.Id = j;
                    DataBase.Add(card);
                    if (card.Answer == -1) DataBaseSell.Add(card);
                    if (card.Answer == 0) DataBaseHold.Add(card);
                    if (card.Answer == 1) DataBaseBuy.Add(card);
                }

                //string serialized = JsonConvert.SerializeObject(card);
            }
        }

        private static void CutDataBase(int sell, int hold, int buy)
        {
            if (sell > DataBaseSell.Count 
                || buy > DataBaseBuy.Count 
                || hold > DataBaseHold.Count)
            {
                Console.WriteLine("CutDataBase Wrong parametrs. max: " 
                    + DataBaseSell.Count + " " 
                    + DataBaseHold.Count + " " 
                    + DataBaseBuy.Count);
                Thread.Sleep(1200);
                return;
            }

            List<Card> dataBase = new List<Card>();
            List<Card> dataBaseSell = new List<Card>();
            List<Card> dataBaseHold = new List<Card>();
            List<Card> dataBaseBuy = new List<Card>();

            for (int i = 0; i < sell; i++)
            {
                dataBaseSell.Add(DataBaseSell[i]);
                dataBase.Add(DataBaseSell[i]);
            }

            for (int i = 0; i < hold; i++)
            {
                dataBaseHold.Add(DataBaseHold[i]);
                dataBase.Add(DataBaseHold[i]);
            }

            for (int i = 0; i < buy; i++)
            {
                dataBaseBuy.Add(DataBaseBuy[i]);
                dataBase.Add(DataBaseBuy[i]);
            }

            DataBase = dataBase;
            DataBaseSell = dataBaseSell;
            DataBaseHold = dataBaseHold;
            DataBaseBuy = dataBaseBuy;
        }

        private static int GetAnswer(int index)
        {
            double closePrice1 = Convert.ToDouble(InputCSV[index].C, CultureInfo.InvariantCulture);
            double closePrice2 = Convert.ToDouble(InputCSV[index + ExpirationTime].C, CultureInfo.InvariantCulture);
            if (closePrice1 - MinimalProfit > closePrice2)
                return -1;
            if (closePrice1 + MinimalProfit < closePrice2)
                return 1;

            //File.AppendAllText(Directory.GetCurrentDirectory() + "\\test2.txt", index + "(" + (index - SizeOfCard + 1) + ")" + " " + (closePrice1 - closePrice2).ToString() + Environment.NewLine);
            return 0;
        }

        private static void WriteCardsInfo()
        {
            List<string> Ids = new List<string>(File.ReadAllLines(Directory.GetCurrentDirectory() + "\\test.txt"));
            foreach (string id in Ids)
            {
                foreach (Card card in DataBase)
                {
                    if (card.Id.ToString() == id)
                    {
                        File.AppendAllText(Directory.GetCurrentDirectory() + "\\cards.json", JsonConvert.SerializeObject(card) + Environment.NewLine);
                    }
                }
            }
        }

        private static void RemoveFromDatabase()
        {
            List<string> Ids = new List<string>(File.ReadAllLines(Directory.GetCurrentDirectory() + "\\test.txt"));
            foreach (string id in Ids)
            {
                List<Card> todel = new List<Card>();
                foreach (Card card in DataBase)
                    if (card.Id.ToString() == id)
                        todel.Add(card);
                foreach (Card card in todel)
                {
                    try
                    {
                        DataBase.Remove(card);
                        DataBaseBuy.Remove(card);
                        DataBaseHold.Remove(card);
                        DataBaseSell.Remove(card);
                    }
                    catch
                    { }
                }
                Thread.Sleep(1);
            }
        }

        private static void SafeExperimentData(string num, string coeff, string seconds_gen)
        {
            File.AppendAllText(Directory.GetCurrentDirectory() + "\\experiment1.txt", num + "," + coeff + "," + seconds_gen + Environment.NewLine);
        }

        private static void MyExperiments()
        {
            int[] m = { 50, 100, 150, 200, 250, 500, 1000 };
            int count = 10;

            foreach (int coeff in m)
            {
                Coeff = coeff;
                for (int num = 1; num <= count; num++)
                {
                    textExperiment = "";
                    ConditionSatisfied = false;
                    outputCount = 3;
                    SizeOfCard = 40;
                    ExpirationTime = 24 * 60; // 1 day
                    Coefficient = 0.01; //Convert.ToDouble(Console.ReadLine()); // for normalization 0.001
                    MinimalProfit = 0.0060; // amount of points as guarantee profit
                                            ///summary regarding Distance
                                            ///it is how much cards will be shown to NN for evaluating fitness.
                                            ///should be more than 1 month if we use AverageMonthlyReturn
                                            ///1 month = 43200 minutes
                    Distance = coeff;
                    //InputCSV = ParsingCSV(Directory.GetCurrentDirectory() + "\\inputs.csv", 1);
                    InputCSV = ParsingCSV(Directory.GetCurrentDirectory() + "\\inputs_format2.csv", 2);

                    Console.WriteLine("Creating DataBase...");
                    CreateDataBase();
                    CutDataBase(200, 200, 200);
                    //RemoveFromDatabase();
                    Console.Clear();
                    Console.WriteLine("DataBaseSell " + DataBaseSell.Count);
                    Console.WriteLine("DataBaseHold " + DataBaseHold.Count);
                    Console.WriteLine("DataBaseBuy  " + DataBaseBuy.Count);
                    //WriteCardsInfo();
                    startExperiment = DateTime.Now;
                    Thread.Sleep(1000);
                    Console.Clear();

                    // Initialise log4net (log to console).
                    XmlConfigurator.Configure(new FileInfo("log4net.properties"));

                    // Experiment classes encapsulate much of the nuts and bolts of setting up a NEAT search.
                    TradersTeacherExperiment experiment = new TradersTeacherExperiment();

                    // Load config XML.
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load("tradersTeacher.config.xml");
                    experiment.Initialize("TradersTeacher", xmlConfig.DocumentElement);

                    // Create evolution algorithm and attach update event.
                    Console.WriteLine("Initialize...");
                    _ea = experiment.CreateEvolutionAlgorithm();
                    _ea.UpdateEvent += new EventHandler(ea_UpdateEvent);

                    // Start algorithm (it will run on a background thread).
                    _ea.StartContinue();
                    Console.Clear();
                    Console.WriteLine("Traders Evolution");

                    // Hit return to quit.
                    while (true)
                    {
                        if (ConditionSatisfied)
                        {
                            Thread.Sleep(6345);
                            SafeExperimentData(num.ToString(), Coeff.ToString(), textExperiment);
                            break;
                        }
                        Thread.Sleep(123);
                    }
                }
            }
        }

        private static void MyExperiment2()
        {
            int[] m = { 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
            int count = 10;

            foreach (int coeff in m)
            {
                Coeff = coeff;
                for (int num = 1; num <= count; num++)
                {
                    textExperiment = "";
                    ConditionSatisfied = false;
                    outputCount = 3;
                    SizeOfCard = 40;
                    ExpirationTime = 24 * 60; // 1 day
                    Coefficient = 0.01; //Convert.ToDouble(Console.ReadLine()); // for normalization 0.001
                    MinimalProfit = 0.0060; // amount of points as guarantee profit
                                            ///summary regarding Distance
                                            ///it is how much cards will be shown to NN for evaluating fitness.
                                            ///should be more than 1 month if we use AverageMonthlyReturn
                                            ///1 month = 43200 minutes
                    Distance = 50;
                    //InputCSV = ParsingCSV(Directory.GetCurrentDirectory() + "\\inputs.csv", 1);
                    InputCSV = ParsingCSV(Directory.GetCurrentDirectory() + "\\inputs_format2.csv", 2);

                    Console.WriteLine("Creating DataBase...");
                    CreateDataBase();
                    CutDataBase(coeff, coeff, coeff);
                    //RemoveFromDatabase();
                    Console.Clear();
                    Console.WriteLine("DataBaseSell " + DataBaseSell.Count);
                    Console.WriteLine("DataBaseHold " + DataBaseHold.Count);
                    Console.WriteLine("DataBaseBuy  " + DataBaseBuy.Count);
                    //WriteCardsInfo();
                    startExperiment = DateTime.Now;
                    Thread.Sleep(1000);
                    Console.Clear();

                    // Initialise log4net (log to console).
                    XmlConfigurator.Configure(new FileInfo("log4net.properties"));

                    // Experiment classes encapsulate much of the nuts and bolts of setting up a NEAT search.
                    TradersTeacherExperiment experiment = new TradersTeacherExperiment();

                    // Load config XML.
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load("tradersTeacher.config.xml");
                    experiment.Initialize("TradersTeacher", xmlConfig.DocumentElement);

                    // Create evolution algorithm and attach update event.
                    Console.WriteLine("Initialize...");
                    _ea = experiment.CreateEvolutionAlgorithm();
                    _ea.UpdateEvent += new EventHandler(ea_UpdateEvent);

                    // Start algorithm (it will run on a background thread).
                    _ea.StartContinue();
                    Console.Clear();
                    Console.WriteLine("Traders Evolution");

                    // Hit return to quit.
                    while (true)
                    {
                        if (ConditionSatisfied)
                        {
                            Thread.Sleep(6345);
                            SafeExperimentData(num.ToString(), Coeff.ToString(), textExperiment);
                            break;
                        }
                        Thread.Sleep(123);
                    }
                }
            }
        }


        private static void StartLearning()
        {
            outputCount = 3;
            SizeOfCard = 40;
            ExpirationTime = 24 * 60; // 1 day
            Coefficient = 0.001; //Convert.ToDouble(Console.ReadLine()); // for normalization 0.001
            MinimalProfit = 20; // amount of points as guarantee profit
                                    ///summary regarding Distance
                                    ///it is how much cards will be shown to NN for evaluating fitness.
                                    ///should be more than 1 month if we use AverageMonthlyReturn
                                    ///1 month = 20 days
            Distance = 24 * 60 * 20;
            InputCSV = ParsingCSV(Directory.GetCurrentDirectory() + "\\inputs.csv", 1);
            //InputCSV = ParsingCSV(Directory.GetCurrentDirectory() + "\\inputs_format2.csv", 2);

            Console.WriteLine("Creating DataBase...");
            CreateDataBase();
            //CutDataBase(200, 200, 200);
            //RemoveFromDatabase();
            Console.Clear();
            Console.WriteLine("DataBaseSell " + DataBaseSell.Count);
            Console.WriteLine("DataBaseHold " + DataBaseHold.Count);
            Console.WriteLine("DataBaseBuy  " + DataBaseBuy.Count);
            //WriteCardsInfo();
            startExperiment = DateTime.Now;
            Thread.Sleep(2000);
            Console.Clear();

            // Initialise log4net (log to console).
            XmlConfigurator.Configure(new FileInfo("log4net.properties"));

            // Experiment classes encapsulate much of the nuts and bolts of setting up a NEAT search.
            TradersTeacherExperiment experiment = new TradersTeacherExperiment();

            // Load config XML.
            XmlDocument xmlConfig = new XmlDocument();
            xmlConfig.Load("tradersTeacher.config.xml");
            experiment.Initialize("TradersTeacher", xmlConfig.DocumentElement);

            // Create evolution algorithm and attach update event.
            Console.WriteLine("Initialize...");
            _ea = experiment.CreateEvolutionAlgorithm();
            _ea.UpdateEvent += new EventHandler(ea_UpdateEvent);

            // Start algorithm (it will run on a background thread).
            _ea.StartContinue();
            Console.Clear();
            Console.WriteLine("Traders Evolution");

            Console.ReadLine();
        }
    }
}
