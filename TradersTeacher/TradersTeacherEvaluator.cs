﻿using Newtonsoft.Json;
using SharpNeat.Core;
using SharpNeat.Phenomes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static TradersTeacher.Data;

namespace TradersTeacher
{
    class TradersTeacherEvaluator : IPhenomeEvaluator<IBlackBox>
    {
        private ulong _evalCount;
        private bool _stopConditionSatisfied;
        private Random rnd = new Random();

        #region IPhenomeEvaluator<IBlackBox> Members

        /// <summary>
        /// Gets the total number of evaluations that have been performed.
        /// </summary>
        public ulong EvaluationCount
        {
            get { return _evalCount; }
        }

        /// <summary>
        /// Gets a value indicating whether some goal fitness has been achieved and that
        /// the the evolutionary algorithm/search should stop. This property's value can remain false
        /// to allow the algorithm to run indefinitely.
        /// </summary>
        public bool StopConditionSatisfied
        {
            get { return _stopConditionSatisfied; }
        }

        /// <summary>
        /// Evaluate the provided IBlackBox against the random set of images and return its fitness score.
        /// Each network gets <distanse> (e.g. 1000) images, one by one, and give answer.
        /// If the answer is right: + 1 point, mistake: 0;
        /// </summary>
        public FitnessInfo Evaluate(IBlackBox box)
        {
            double FitnessScore = 0;
            NeatTrader neatTrader = new NeatTrader(box);

            //FitnessScore = FitnessMethod_2(neatTrader);
            FitnessScore = FitnessMethod_2(neatTrader);


            // Update the evaluation counter.
            _evalCount++;

            if (FitnessScore >= (Distance * 3) + 100000)
            {
                _stopConditionSatisfied = true;
                ConditionSatisfied = true;
            }

            // Return the fitness score
            return new FitnessInfo(FitnessScore, FitnessScore);
        }

        /// <summary>
        /// Reset the internal state of the evaluation scheme if any exists.
        /// This method does nothing.
        /// </summary>
        public void Reset()
        {
        }

        private Card GetCard(int type = 2) // type: -1 - sell; 0 - hold; 1 - buy; 2 - random;
        {
            if (type == -1) return DataBaseSell[rnd.Next(0, DataBaseSell.Count)];
            if (type == 0) return DataBaseHold[rnd.Next(0, DataBaseHold.Count)];
            if (type == 1) return DataBaseBuy[rnd.Next(0, DataBaseBuy.Count)];
            return DataBase[rnd.Next(0, DataBase.Count)];
        }

        private void ShowInfo(double fitness, int CardS, int CardH, int CardB, int NNS, int NNH, int NNB, int good, int bad)
        {
            Console.WriteLine(String.Format("Card: {0} {1} {2}", CardS, CardH, CardB) + Environment.NewLine
                       + String.Format("  NN: {0} {1} {2}", NNS, NNH, NNB) + Environment.NewLine
                       + "FitnessNow: " + fitness + Environment.NewLine
                       + "FitnessMAX: " + (CardS + CardB + CardH) + Environment.NewLine
                       + "Good: " + good + Environment.NewLine
                       + "Bad: " + bad + Environment.NewLine
                       + "Success: " + Math.Round(Convert.ToDouble(good, CultureInfo.InvariantCulture) / (Convert.ToDouble(good, CultureInfo.InvariantCulture) + Convert.ToDouble(bad, CultureInfo.InvariantCulture)) * 100, 2) + " %" + Environment.NewLine);
        }
        #endregion
        
        private double FitnessMethod_2(NeatTrader neatTrader)
        {
            // Fitness = (NumberOfWinners - NumberOfLosers) * SortinoRatio * AverageMonthlyReturn;
            double FitnessScore = 0;
            int NumberOfWinners = 0;
            int NumberOfLosers = 0;
            double Deposit = 100;
            double initialDepo = 100; // to calculate daily return
            List<double> MonthlyReturn = new List<double>(); // Profitability for each month (%) 
            List<double> DailyReturn = new List<double>(); // Profitability for each day (%)
            double SortinoRatio = 0;
            double SharpRatio = 0;
            double Profitability = 0; // (%)
            double AverageDailyReturn = 0;
            double AverageMonthlyReturn = 0;
            double RiskFreeRate = 0; // minimal profitability(%)
            double StandardDeviationOfNegativeAssetReturns = 0;
            double StandardDeviation = 0;
            int month = 0; // 1 month = 20 days
            int day = 0; // 1 day = 1440 min
            int minute = 0; // 1 minute = 1 card

            for (int i = 0; i < Distance; i++)  // Distance = 1 month = 43200 min
            {
                Card card = GetCard(); // get random card
                int answer = neatTrader.GetAnswer(card); // show the card to NN and get answer

                // check the answer
                if (answer != 0) // pass the "hold" answers
                {
                    if (card.Answer == answer)
                    {
                        Deposit++;
                        NumberOfWinners++;
                    }
                    else
                    {
                        Deposit--;
                        NumberOfLosers++;
                    }
                }

                if (minute == 1439) // 1 day
                {
                    minute = 0;
                    day++;

                    // do something each day
                    double daily_profit = Deposit - initialDepo;
                    DailyReturn.Add(daily_profit/initialDepo*100); // %

                    if (day == 20) // 1 month
                    {
                        day = 0;
                        month++;

                        // do something each month
                        double monthly_profit =0;
                        for (int y = DailyReturn.Count - 1; y >= 0; y--)
                            monthly_profit += DailyReturn[y];
                        MonthlyReturn.Add(monthly_profit); // %
                    }
                    Deposit = initialDepo;
                }                

                minute++;
            }

            foreach (double pf in DailyReturn)
                Profitability += pf;

            AverageDailyReturn = Profitability / DailyReturn.Count;

            double summ = 0;
            foreach (double pf in MonthlyReturn)
                summ += pf;
            AverageMonthlyReturn = summ / MonthlyReturn.Count;
            
            List<double> delta = new List<double>();
            foreach(double pf in DailyReturn)
                delta.Add(pf - AverageDailyReturn);
            double sum = 0;
            foreach (double d in delta)
                sum += Math.Pow(d, 2);

            StandardDeviation = Math.Sqrt(sum/delta.Count);
            SharpRatio = Profitability / StandardDeviation;
            
            FitnessScore = (NumberOfWinners - NumberOfLosers) * SharpRatio * AverageMonthlyReturn;

            if (Double.IsNaN(FitnessScore))
                FitnessScore = 0;

            //AverageMonthlyReturn = (NumberOfWinners - NumberOfLosers) / (NumberOfWinners + NumberOfLosers) * 100;
            //StandardDeviationOfNegativeAssetReturns = 

            return  FitnessScore / 100000000 + 10000; //  "/ 100000000 + 10000" - in order to get positive fitness, because SharpNEAT dislike negative fitness
        }

        private double FitnessMethod_1(NeatTrader neatTrader)
        {
            double FitnessScore = 1000;

            int CardS = 0;
            int CardH = 0;
            int CardB = 0;
            int NNS = 0;
            int NNH = 0;
            int NNB = 0;
            int good = 0;
            int bad = 0;
            List<int> badIds = new List<int>();

            // Show cards to NeatTrader
            for (int i = 0; i < Distance; i++)
            {
                Card card;
                int answer;

                CardS++;
                card = GetCard(-1);
                answer = neatTrader.GetAnswer(card);
                if (answer == -1) { good++; FitnessScore++; NNS++; }
                if (answer == 0) { NNH++; }
                if (answer == 1) { bad++; badIds.Add(card.Id); NNB++; FitnessScore--; }

                CardH++;
                card = GetCard(0);
                answer = neatTrader.GetAnswer(card);
                if (answer == -1) { bad++; badIds.Add(card.Id); NNS++; FitnessScore--; }
                if (answer == 0) { NNH++; }
                if (answer == 1) { bad++; badIds.Add(card.Id); NNB++; FitnessScore--; }

                CardB++;
                card = GetCard(1);
                answer = neatTrader.GetAnswer(card);
                if (answer == 1) { good++; FitnessScore++; NNB++; }
                if (answer == 0) { NNH++; }
                if (answer == -1) { bad++; badIds.Add(card.Id); NNS++; FitnessScore--; }
            }

            //if (fitness > 1092) ShowInfo(fitness, CardS, CardH, CardB, NNS, NNH, NNB, good, bad);

            //if (fitness >= 140)
            //{
            //    foreach (int u in badIds)
            //        write(u);
            //    Thread.Sleep(1);
            //}

            return FitnessScore;
        }

        private double FitnessMethod_0(NeatTrader neatTrader)
        {
            double FitnessScore = 0;

            int CardS = 0;
            int CardH = 0;
            int CardB = 0;
            int NNS = 0;
            int NNH = 0;
            int NNB = 0;
            int good = 0;
            int bad = 0;
            List<int> badIds = new List<int>();

            // Show cards to NeatTrader
            for (int i = 0; i < Distance; i++)
            {
                Card card;
                int answer;

                CardS++;
                card = GetCard(-1);
                answer = neatTrader.GetAnswer(card);
                if (answer == -1) NNS++;
                if (answer == 0) NNH++;
                if (answer == 1) NNB++;
                if (card.Answer == answer) { good++; FitnessScore++; }
                else
                {
                    badIds.Add(card.Id);
                    bad++;
                }

                CardH++;
                card = GetCard(0);
                answer = neatTrader.GetAnswer(card);
                if (answer == -1) NNS++;
                if (answer == 0) NNH++;
                if (answer == 1) NNB++;
                if (card.Answer == answer) { good++; FitnessScore++; }
                else
                {
                    badIds.Add(card.Id);
                    bad++;
                }

                CardB++;
                card = GetCard(1);
                answer = neatTrader.GetAnswer(card);
                if (answer == -1) NNS++;
                if (answer == 0) NNH++;
                if (answer == 1) NNB++;
                if (card.Answer == answer) { good++; FitnessScore++; }
                else
                {
                    badIds.Add(card.Id);
                    bad++;
                }
            }

            //if (FitnessScore >= 100) ShowInfo(FitnessScore, CardS, CardH, CardB, NNS, NNH, NNB, good, bad);

            //if (fitness >= 140)
            //{
            //    foreach (int u in badIds)
            //        write(u);
            //    Thread.Sleep(1);
            //}

            return FitnessScore;
        }
    }
}
