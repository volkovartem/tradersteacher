﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SharpNeat.Phenomes;
using static TradersTeacher.Data;

namespace TradersTeacher
{
    public class NeatTrader
    {
        /// <summary>
        /// The neural network that this player uses to make its decision.
        /// </summary>
        public IBlackBox Brain { get; set; }

        /// <summary>
        /// Creates a new NEAT player with the specified brain.
        /// </summary>
        public NeatTrader(IBlackBox brain)
        {
            Brain = brain;
        }

        public int GetAnswer(Card card)
        {
            // Clear the network
            Brain.ResetState();

            // Set input array for the network
            int i = 0;
            foreach (Candle candle in card.Candles)
            {
                Brain.InputSignalArray[i++] = Convert.ToDouble(candle.T, CultureInfo.InvariantCulture);
                Brain.InputSignalArray[i++] = Convert.ToDouble(candle.O, CultureInfo.InvariantCulture);
                Brain.InputSignalArray[i++] = Convert.ToDouble(candle.H, CultureInfo.InvariantCulture);
                Brain.InputSignalArray[i++] = Convert.ToDouble(candle.L, CultureInfo.InvariantCulture);
                Brain.InputSignalArray[i++] = Convert.ToDouble(candle.C, CultureInfo.InvariantCulture);
                Brain.InputSignalArray[i++] = Convert.ToDouble(candle.V, CultureInfo.InvariantCulture);
            }

            // Activate the network
            Brain.Activate();

            // Find the highest-scoring value
            int number = 0;
            double max = double.MinValue;
            for (int j = 0; j < outputCount; j++)
            {
                // Set the score for this number
                double score = Brain.OutputSignalArray[j];

                // If this number has a higher score than any we've found, set it to the current best.
                if (max < score)
                {
                    number = j;
                    max = score;
                }
            }
            return number - 1;
        }
    }
}
