﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TradersTeacher
{
    public class Data
    {
        public static int outputCount { get; set; }
        public static int SizeOfCard { get; set; }
        public static int ExpirationTime { get; set; }
        public static double MinimalProfit { get; set; }
        public static int Distance { get; set; }
        public static double Coefficient { get; set; }
        public static List<Card> DataBase { get; set; }
        public static List<Card> DataBaseBuy { get; set; }
        public static List<Card> DataBaseSell { get; set; }
        public static List<Card> DataBaseHold { get; set; }
        public static List<Candle> InputCSV { get; set; }

        #region For my experiments
        public static bool ConditionSatisfied { get; set; }
        public static string textExperiment { get; set; }
        public static DateTime startExperiment { get; set; }
        public static int Coeff {get; set;}
        #endregion

        public static Mutex mtx = new Mutex();

        public static string Normalize(string data)
        {
            return Math.Tanh(Convert.ToDouble(data, CultureInfo.InvariantCulture) * Coefficient).ToString();
        }

        public static string NormalizeTime(string data)
        {
            int h = Convert.ToInt32(data.Split(':')[0]);
            int m = Convert.ToInt32(data.Split(':')[1]);
            return (((h * 60) + m) * 0.000694).ToString();
        }

        public static void write(int d)
        {
            mtx.WaitOne();
            File.AppendAllText(Directory.GetCurrentDirectory() + "\\test0.txt", d.ToString() + Environment.NewLine);
            mtx.ReleaseMutex();
        }
    }
}
