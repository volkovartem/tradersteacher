﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradersTeacher
{
    public class Candle
    {
        public string T { get; set; }
        public string O { get; set; }
        public string H { get; set; }
        public string L { get; set; }
        public string C { get; set; }
        public string V { get; set; }
    }

    public class Card
    {
        public int Id { get; set; }
        public List<Candle> Candles { get; set; }
        public int Answer { get; set; }
    }
}
